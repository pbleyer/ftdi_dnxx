"""
Python FTDI D3XX module
"""

#	Copyright (C) 2019 Pablo Bleyer Kocik
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from ctypes import *
from enum import IntEnum


DEVICE_DESCRIPTOR_TYPE = 0x01
CONFIGURATION_DESCRIPTOR_TYPE = 0x02
STRING_DESCRIPTOR_TYPE = 0x03
INTERFACE_DESCRIPTOR_TYPE = 0x04

def IS_SELF_POWERED(bmAttributes):
	return bmAttributes == 0x40

def IS_BUS_POWERED(bmAttributes):
	return bmAttributes != 0x40

def IS_REMOTE_WAKEUP_ENABLED(bmAttributes):
	return bmAttributes == 0x20

def IS_READ_PIPE(ucPipeID):
	return bool(ucPipeID & 0x80)

def IS_WRITE_PIPE(ucPipeID):
	return not bool(ucPipeID & 0x80)

def IS_BULK_PIPE(ucPipeType):
	return ucPipeType == 2

def IS_INTERRUPT_PIPE(ucPipeType):
	return ucPipeType == 3

def IS_ISOCHRONOUS_PIPE(ucPipeType):
	return False

RESERVED_INTERFACE_INDEX = 0x0
RESERVED_PIPE_INDEX_SESSION = 0x0
RESERVED_PIPE_INDEX_NOTIFICATION = 0x1
RESERVED_PIPE_SESSION = 0x1
RESERVED_PIPE_NOTIFICATION = 0x81

OPEN_BY_SERIAL_NUMBER = 0x00000001
OPEN_BY_DESCRIPTION = 0x00000002
OPEN_BY_LOCATION = 0x00000004
OPEN_BY_GUID = 0x00000008
OPEN_BY_INDEX = 0x00000010

LIST_ALL = 0x20000000
LIST_BY_INDEX = 0x40000000
LIST_NUMBER_ONLY = 0x80000000

GPIO_DIRECTION_IN = 0
GPIO_DIRECTION_OUT = 1
GPIO_VALUE_LOW = 0
GPIO_VALUE_HIGH = 1
GPIO_0 = 0
GPIO_1 = 1

HANDLE = c_void_p
PHANDLE = POINTER(c_void_p)

class STATUS(IntEnum):
	OK = 0
	INVALID_HANDLE = 1
	DEVICE_NOT_FOUND = 2
	DEVICE_NOT_OPENED = 3
	IO_ERROR = 4
	INSUFFICIENT_RESOURCES = 5
	INVALID_PARAMETER = 6
	INVALID_BAUD_RATE = 7
	DEVICE_NOT_OPENED_FOR_ERASE = 8
	DEVICE_NOT_OPENED_FOR_WRITE = 9
	FAILED_TO_WRITE_DEVICE = 10
	EEPROM_READ_FAILED = 11
	EEPROM_WRITE_FAILED = 12
	EEPROM_ERASE_FAILED = 13
	EEPROM_NOT_PRESENT = 14
	EEPROM_NOT_PROGRAMMED = 15
	INVALID_ARGS = 16
	NOT_SUPPORTED = 17

	NO_MORE_ITEMS = 18
	TIMEOUT = 19
	OPERATION_ABORTED = 20
	RESERVED_PIPE = 21
	INVALID_CONTROL_REQUEST_DIRECTION = 22
	INVALID_CONTROL_REQUEST_TYPE = 23
	IO_PENDING = 24
	IO_INCOMPLETE = 25
	HANDLE_EOF = 26
	BUSY = 27
	NO_SYSTEM_RESOURCES = 28
	DEVICE_LIST_NOT_READY = 29
	DEVICE_NOT_CONNECTED = 30
	INCORRECT_DEVICE_PATH = 31

	OTHER_ERROR = 32

	def __init__(self, v):
		self._as_parameter = int(v)
	@classmethod
	def from_param(cls, obj):
		return int(obj)


# Make enums accessible at module level for convenience
globals().update(STATUS.__members__)


def SUCCESS(status):
	return status == STATUS.OK

def FAILED(status):
	return status != STATUS.OK

class PIPE_TYPE(IntEnum):
	CONTROL = 0
	ISOCHRONOUS = 1
	BULK = 2
	INTERRUPT = 3

	Control = 0
	Isochronous = 1
	Bulk = 2
	Interrupt = 3

class COMMON_DESCRIPTOR(Structure):
	__fields__ = [
		('bLength', c_ubyte),
		('bDescriptorType', c_ubyte),
	]

PCOMMON_DESCRIPTOR = POINTER(COMMON_DESCRIPTOR)

class DEVICE_DESCRIPTOR(Structure):
	__fields__ = [
		('bLength', c_ubyte),
		('bDescriptorType', c_ubyte),
		('bcdUSB', c_ushort),
		('bDeviceClass', c_ubyte),
		('bDeviceSubClass', c_ubyte),
		('bDeviceProtocol', c_ubyte),
		('bMaxPacketSize0', c_ubyte),
		('idVendor', c_ushort),
		('idProduct', c_ushort),
		('bcdDevice', c_ushort),
		('iManufacturer', c_ubyte),
		('iProduct', c_ubyte),
		('iSerialNumber', c_ubyte),
		('bNumConfigurations', c_ubyte),
	]

PDEVICE_DESCRIPTOR = POINTER(DEVICE_DESCRIPTOR)

class CONFIGURATION_DESCRIPTOR(Structure):
	__fields__ = [
		('bLength', c_ubyte),
		('bDescriptorType', c_ubyte),
		('wTotalLength', c_ushort),
		('bNumInterfaces', c_ubyte),
		('bConfigurationValue', c_ubyte),
		('iConfiguration', c_ubyte),
		('bmAttributes', c_ubyte),
		('MaxPower', c_ubyte),
	]

PCONFIGURATION_DESCRIPTOR = POINTER(CONFIGURATION_DESCRIPTOR)

class INTERFACE_DESCRIPTOR(Structure):
	__fields__ = [
		('bLength', c_ubyte),
		('bDescriptorType', c_ubyte),
		('bInterfaceNumber', c_ubyte),
		('bAlternateSetting', c_ubyte),
		('bNumEndpoints', c_ubyte),
		('bInterfaceClass', c_ubyte),
		('bInterfaceSubClass', c_ubyte),
		('bInterfaceProtocol', c_ubyte),
		('iInterface', c_ubyte),
	]

PINTERFACE_DESCRIPTOR = POINTER(INTERFACE_DESCRIPTOR)

class STRING_DESCRIPTOR(Structure):
	__fields__ = [
		('bLength', c_ubyte),
		('bDescriptorType', c_ubyte),
		('szString', c_wchar * 256),
	]

PSTRING_DESCRIPTOR = POINTER(STRING_DESCRIPTOR)

class PIPE_INFORMATION(Structure):
	__fields__ = [
		('PipeType', c_int), # FT_PIPE_TYPE enum
		('PipeId', c_ubyte),
		('MaximumPacketSize', c_ushort),
		('Interval', c_ubyte),
	]

PPIPE_INFORMATION = POINTER(PIPE_INFORMATION)

class SETUP_PACKET(Structure):
	__fields__ = [
		('RequestType', c_ubyte),
		('Request', c_ubyte),
		('Value', c_ushort),
		('Index', c_ushort),
		('Length', c_ushort),
	]

PSETUP_PACKET = POINTER(SETUP_PACKET)

class E_NOTIFICATION_CALLBACK_TYPE(IntEnum):
	DATA = 0
	GPIO = 1

class NOTIFICATION_CALLBACK_INFO_DATA(Structure):
	__fields__ = [
		('ulRecvNotificationLength', c_ulong), # ULONG
		('ucEndpointNo', c_ubyte),
	]

class NOTIFICATION_CALLBACK_INFO_GPIO(Structure):
	__fields__ = [
		('bGPIO0', c_int), # BOOL
		('bGPIO1', c_int), # BOOL
	]

# typedef VOID(*NOTIFICATION_CALLBACK)(PVOID pvCallbackContext, E_NOTIFICATION_CALLBACK_TYPE eCallbackType, PVOID pvCallbackInfo);
NOTIFICATION_CALLBACK = CFUNCTYPE(None, c_void_p, c_int, c_void_p)

class CONFIGURATION_FLASH_ROM_BIT(IntEnum):
	ROM = 0
	MEMORY_NOTEXIST = 1
	CUSTOMDATA_INVALID = 2
	CUSTOMDATACHKSUM_INVALID = 3
	CUSTOM = 4
	GPIO_INPUT = 5
	GPIO_0 = 6
	GPIO_1 = 7

class CONFIGURATION_BATCHG_BIT_OFFSET(IntEnum):
	DCP = 6
	CDP = 4
	SDP = 2
	DEF = 0
	MASK = 3

class CONFIGURATION_FIFO_CLK(IntEnum):
	_100 = 0
	_66 = 1
	COUNT = 2

class CONFIGURATION_FIFO_MODE(IntEnum):
	_245 = 0
	_600 = 1
	COUNT = 2

class CONFIGURATION_CHANNEL_CONFIG(IntEnum):
	_4 = 0
	_2 = 1
	_1 = 2
	_1_OUTPIPE = 3
	_1_INPIPE = 4
	COUNT = 5

class CONFIGURATION_OPTIONAL_FEATURE_SUPPORT(IntEnum):
	DISABLEALL = 0
	ENABLEBATTERYCHARGING = (0x1 << 0)
	DISABLECANCELSESSIONUNDERRUN = (0x1 << 1)
	ENABLENOTIFICATIONMESSAGE_INCH1 = (0x1 << 2)
	ENABLENOTIFICATIONMESSAGE_INCH2 = (0x1 << 3)
	ENABLENOTIFICATIONMESSAGE_INCH3 = (0x1 << 4)
	ENABLENOTIFICATIONMESSAGE_INCH4 = (0x1 << 5)
	ENABLENOTIFICATIONMESSAGE_INCHALL = (0xF << 2)
	DISABLEUNDERRUN_INCH1 = (0x1 << 6)
	DISABLEUNDERRUN_INCH2 = (0x1 << 7)
	DISABLEUNDERRUN_INCH3 = (0x1 << 8)
	DISABLEUNDERRUN_INCH4 = (0x1 << 9)
	DISABLEUNDERRUN_INCHALL = (0xF << 6)
	ENABLEALL = 0xFFFF

# Alias
CONFIGURATION_OPTIONAL_FEATURE = CONFIGURATION_OPTIONAL_FEATURE_SUPPORT

class CONFIGURATION_DEFAULT(IntEnum):
	VENDORID = 0x0403
	PRODUCTID_600 = 0x601E
	PRODUCTID_601 = 0x601F
	POWERATTRIBUTES = 0xE0
	POWERCONSUMPTION = 0x60
	FIFOCLOCK = CONFIGURATION_FIFO_CLK._100
	FIFOMODE = CONFIGURATION_FIFO_MODE._600
	CHANNELCONFIG = CONFIGURATION_CHANNEL_CONFIG._4
	OPTIONALFEATURE = CONFIGURATION_OPTIONAL_FEATURE.DISABLEALL
	BATTERYCHARGING = 0xE4
	BATTERYCHARGING_TYPE_DCP = 0x3
	BATTERYCHARGING_TYPE_CDP = 0x2
	BATTERYCHARGING_TYPE_SDP = 0x1
	BATTERYCHARGING_TYPE_OFF = 0x0
	FLASHDETECTION = 0x0
	MSIOCONTROL = 0x10800
	GPIOCONTROL = 0x0

class FT60XCONFIGURATION(Structure):
	__fields__ = [
		('VendorID', c_ushort),
		('ProductID', c_ushort),
		('StringDescriptors[128]', c_char * 128),
		('bInterval', c_ubyte),
		('PowerAttributes', c_ubyte),
		('PowerConsumption', c_ushort),
		('Reserved2', c_ubyte),
		('FIFOClock', c_ubyte),
		('FIFOMode', c_ubyte),
		('ChannelConfig', c_ubyte),
		('OptionalFeatureSupport', c_ushort),
		('BatteryChargingGPIOConfig', c_ubyte),
		('FlashEEPROMDetection', c_ubyte),
		('MSIO_Control', c_ulong),
		('GPIO_Control', c_ulong),
	]

PFT60XCONFIGURATION = POINTER(FT60XCONFIGURATION)

class DEVICE(IntEnum):
	UNKNOWN = 3
	FT600 = 600
	FT601 = 601

class FLAGS(IntEnum):
	OPENED = 1
	HISPEED = 2
	SUPERSPEED = 4

class DEVICE_LIST_INFO_NODE(Structure):
	__fields__ = [
		('Flags', c_ulong),
		('Type', c_ulong),
		('ID', c_ulong),
		('LocId', c_uint), # DWORD
		('SerialNumber', c_char * 16),
		('Description', c_char * 32),
		('ftHandle', HANDLE),
	]
