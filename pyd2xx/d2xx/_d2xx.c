/*
	d2xx � FTDI2XX interface Python module
*/

/*
	Copyright (C) 2007 Pablo Bleyer Kocik

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "_d2xx.h"

/* Method table */
static PyMethodDef ftobj_methods[] = {
	{"close", (PyCFunction)ftobj_Close, METH_VARARGS, "Close an open device."},
	{"read", (PyCFunction)ftobj_Read, METH_VARARGS, "Read data from the device."},
	{"write", (PyCFunction)ftobj_Write, METH_VARARGS, "Write data to the device."},
	{"ioctl", (PyCFunction)ftobj_IoCtl, METH_VARARGS, ""},
	{"setBaudRate", (PyCFunction)ftobj_SetBaudRate, METH_VARARGS, "Sets the baud rate for the device."},
	{"setDivisor", (PyCFunction)ftobj_SetDivisor, METH_VARARGS, "Sets the baud rate for the device; used to set non-standard baud rates."},
	{"setDataCharacteristics", (PyCFunction)ftobj_SetDataCharacteristics, METH_VARARGS, "Sets the data characteristics for the device."},
	{"setFlowControl", (PyCFunction)ftobj_SetFlowControl, METH_VARARGS, "Sets the flow control for the device."},
	{"resetDevice", (PyCFunction)ftobj_ResetDevice, METH_VARARGS, "Sends a reset command to the device."},
	{"setDtr", (PyCFunction)ftobj_SetDtr, METH_VARARGS, "Sets the Data Terminal Ready control signal."},
	{"clrDtr", (PyCFunction)ftobj_ClrDtr, METH_VARARGS, "Clears the Data Terminal Ready control signal."},
	{"setRts", (PyCFunction)ftobj_SetRts, METH_VARARGS, "Sets the Request To Send control signal."},
	{"clrRts", (PyCFunction)ftobj_ClrRts, METH_VARARGS, "Clears the Request To Send control signal."},
	{"getModemStatus", (PyCFunction)ftobj_GetModemStatus, METH_VARARGS, "Gets the modem status from the device."},
	{"setChars", (PyCFunction)ftobj_SetChars, METH_VARARGS, "Sets the special characters for the device."},
	{"purge", (PyCFunction)ftobj_Purge, METH_VARARGS, "Purges receive and transmit buffers in the device."},
	{"setTimeouts", (PyCFunction)ftobj_SetTimeouts, METH_VARARGS, "Sets the read and write timeouts for the device."},
	{"setDeadmanTimeout", (PyCFunction)ftobj_SetDeadmanTimeout, METH_VARARGS, "Sets the deadman timeout for the device."},
	{"getQueueStatus", (PyCFunction)ftobj_GetQueueStatus, METH_VARARGS, "Gets the number of characters in the receive queue."},
	{"setEventNotification", (PyCFunction)ftobj_SetEventNotification, METH_VARARGS, "Sets conditions for event notification."},
	{"getStatus", (PyCFunction)ftobj_GetStatus, METH_VARARGS, "Gets the device status including number of characters in the receive queue, number of characters in the transmit queue, and the current event status."},
	{"setBreakOn", (PyCFunction)ftobj_SetBreakOn, METH_VARARGS, "Sets the break condition for the device."},
	{"setBreakOff", (PyCFunction)ftobj_SetBreakOff, METH_VARARGS, "Resets the break condition for the device."},
	{"setWaitMask", (PyCFunction)ftobj_SetWaitMask, METH_VARARGS, ""},
	{"waitOnMask", (PyCFunction)ftobj_WaitOnMask, METH_VARARGS, ""},
	{"getEventStatus", (PyCFunction)ftobj_GetEventStatus, METH_VARARGS, ""},

	{"getLatencyTimer", (PyCFunction)ftobj_GetLatencyTimer, METH_VARARGS, "Get the current value of the latency timer."},
	{"setLatencyTimer", (PyCFunction)ftobj_SetLatencyTimer, METH_VARARGS, "Set the latency timer."},
	{"getBitMode", (PyCFunction)ftobj_GetBitMode, METH_VARARGS, "Get the current value of the bit mode."},
	{"setBitMode", (PyCFunction)ftobj_SetBitMode, METH_VARARGS, "Set the value of the bit mode."},
	{"setUSBParameters", (PyCFunction)ftobj_SetUSBParameters, METH_VARARGS, "Set the USB request transfer size."},
	{"getDeviceInfo", (PyCFunction)ftobj_GetDeviceInfo, METH_VARARGS, "Gets a pointer to a device information structure."},

	{"stopInTask", (PyCFunction)ftobj_StopInTask, METH_VARARGS, ""},
	{"restartInTask", (PyCFunction)ftobj_RestartInTask, METH_VARARGS, ""},
	{"setResetPipeRetryCount", (PyCFunction)ftobj_SetResetPipeRetryCount, METH_VARARGS, ""},
	{"resetPort", (PyCFunction)ftobj_ResetPort, METH_VARARGS, "Send a reset command to the port."},
	{"cyclePort", (PyCFunction)ftobj_CyclePort, METH_VARARGS, "Send a cycle command to the USB port."},
	{"getDriverVersion", (PyCFunction)ftobj_GetDriverVersion, METH_VARARGS, "Returns the D2XX driver version number."},
	{"getComPortNumber", (PyCFunction)ftobj_GetComPortNumber, METH_VARARGS, "Returns the device COM port number."},

	{"eeProgram", (PyCFunction)ftobj_EE_Program, METH_VARARGS, "Program the EEPROM."},
	{"eeRead", (PyCFunction)ftobj_EE_Read, METH_VARARGS, "Read the contents of the EEPROM."},
	{"eeUARead", (PyCFunction)ftobj_EE_UARead, METH_VARARGS, "Read the contents of the EEUA."},
	{"eeUAWrite", (PyCFunction)ftobj_EE_UAWrite, METH_VARARGS, "Write data into the EEUA."},
	{"eeUASize", (PyCFunction)ftobj_EE_UASize, METH_VARARGS, "Get size of EEUA."},

	{"eeReadConfig", (PyCFunction)ftobj_EE_ReadConfig, METH_VARARGS, "Read the contents of the EE configuration."},
	{"eeWriteConfig", (PyCFunction)ftobj_EE_WriteConfig, METH_VARARGS, "Write data into the EE configuration."},
	{"eeReadEcc", (PyCFunction)ftobj_EE_ReadEcc, METH_VARARGS, "Read the contents of the EE ECC."},
	{"getQueueStatusEx", (PyCFunction)ftobj_GetQueueStatusEx, METH_VARARGS, "Gets the number of characters in the receive queue (Ex)."},

//#ifdef WIN32_API
//	{"w32ReadFile", (PyCFunction)ftobj_W32_ReadFile, METH_VARARGS, "Read data from the device."},
//#endif

	{NULL, NULL, 0, NULL}
};

static PyObject*
ftobj_alloc(FT_HANDLE h) {
	Ftobj *fo;

	fo = PyObject_New(Ftobj, &Ftobj_Type);
	if (!fo) return PyErr_NoMemory();

	fo->status = Ftobj_OPEN;
	fo->handle = h;

	return (PyObject*)fo;
}

static void
ftobj_dealloc(Ftobj *fo) {
	if (fo->status & Ftobj_OPEN) FT_Close(fo->handle);
	PyObject_Del(fo);
}

//	PVOID pArg1,
//	PVOID pArg2,
//	DWORD Flags
static PyObject*
ftobj_ListDevices(Ftobj *fo, PyObject *ao) {
	PyObject *result = NULL;
	int r = 0;
	FT_STATUS s;
	volatile DWORD n;
	// char **ba = NULL, *bd = NULL;
	unsigned i, l; // max devices
	char *ba[MAX_DEVICES+1];
	char bd[MAX_DEVICES*DESCRIPTION_SIZE];

	// when null, the default is to open by serial number
	if (!PyArg_ParseTuple(ao, "|i:listDevices", &r)) return NULL;
	if (r == 0) r = FT_OPEN_BY_SERIAL_NUMBER;

	// search for number of devices first
	Py_BEGIN_ALLOW_THREADS
	s = FT_ListDevices(&n, NULL, FT_LIST_NUMBER_ONLY);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	if (!n) return PyTuple_New(0); // no devices found

	l = (unsigned)n;
	l = (l<=MAX_DEVICES) ? l : MAX_DEVICES;

	for (i=0; i<l; ++i) ba[i] = bd + i*DESCRIPTION_SIZE;
	ba[l] = NULL;

	/*
	s = FT_ListDevices(ba, &n, FT_LIST_ALL | (DWORD)r);
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}
	*/

	for (i=0; i<l; ++i) {
		Py_BEGIN_ALLOW_THREADS
		s = FT_ListDevices(i, ba[i], FT_LIST_BY_INDEX | (DWORD)r);
		Py_END_ALLOW_THREADS

		if (!FT_SUCCESS(s)) {
			if (s == FT_INVALID_HANDLE) {
				// error opening device or device already opened
				// strncpy(ba[i], "(error/open)", DESCRIPTION_SIZE);
				ba[i] = NULL;
			}
			else {
				PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
				return NULL;
			}
		}
	}

	result = PyTuple_New(l);
	if (result == NULL) {
		PyErr_SetNone(PyExc_MemoryError);
		return NULL;
	}

	for (i=0; i<l; ++i) {
		if (ba[i]) // device was successfully listed
			PyTuple_SetItem(result, i, PyString_FromString(ba[i]));
		else
			// return a None object, as suggested by Jon Kuhn (kuhn at mobiusmicro dot com)
			PyTuple_SetItem(result, i, Py_None);
	}

	return result;
}

#ifndef WIN32
// DWORD dwVID,
// DWORD dwPID
static PyObject*
ftobj_SetVIDPID(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	DWORD v, p;

	if (!PyArg_ParseTuple(ao, "ii:setVIDPID", &v, &p)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetVIDPID(v, p);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

static PyObject*
ftobj_GetVIDPID(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	DWORD v, p;

	if (!PyArg_ParseTuple(ao, ":getVIDPID")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_GetVIDPID(&v, &p);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return Py_BuildValue("(ii)", (int)v, (int)p);
}
#endif

static PyObject*
ftobj_GetLibraryVersion(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	DWORD m;

	if (!PyArg_ParseTuple(ao, ":getLibraryVersion")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_GetLibraryVersion(&m);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong((int)m);
}

static PyObject*
ftobj_Rescan(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;

	if (!PyArg_ParseTuple(ao, ":rescan")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_Rescan();
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

static PyObject*
ftobj_Reload(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	DWORD v, p;

	if (!PyArg_ParseTuple(ao, "ii:reload", &v, &p)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_Reload((WORD)v, (WORD)p);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

static PyObject*
ftobj_CreateDeviceInfoList(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	DWORD m;

	if (!PyArg_ParseTuple(ao, ":createDeviceInfoList")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_CreateDeviceInfoList(&m);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong((int)m);
}

// DWORD dwIndex
// LPDWORD lpdwFlags
// LPDWORD lpdwType
// LPDWORD lpdwID
// LPDWORD lpdwLocId
// PCHAR pcSerialNumber
// PCHAR pcDescription,
// FT_HANDLE *ftHandle
static PyObject*
ftobj_GetDeviceInfoDetail(Ftobj *fo, PyObject *ao) {
	PyObject *result = NULL;
	int dn;
	FT_STATUS s;
	DWORD f = 0, t = 0, i = 0, l = 0;
	char n[256], d[256];
	FT_HANDLE h = 0;

	if (!PyArg_ParseTuple(ao, "i:getDeviceInfoDetail", &dn)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_GetDeviceInfoDetail(dn, &f, &t, &i, &l, n, d, &h);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	result = PyDict_New();
	if (!result) return PyErr_NoMemory();

	PyDict_SetItemString(result, "index", PyInt_FromLong((int)dn));
	PyDict_SetItemString(result, "flags", PyInt_FromLong((int)f));
	PyDict_SetItemString(result, "type", PyInt_FromLong((int)t));
	PyDict_SetItemString(result, "id", PyInt_FromLong((int)i));
	PyDict_SetItemString(result, "location", PyInt_FromLong((int)l));
	PyDict_SetItemString(result, "serial", PyString_FromString(n));
	PyDict_SetItemString(result, "description", PyString_FromString(d));
	PyDict_SetItemString(result, "handle", PyInt_FromLong((int)h));

	return result;
}


//	int deviceNumber,
//	FT_HANDLE *pHandle
static PyObject*
ftobj_Open(Ftobj *fo, PyObject *ao) {
	int dn;
	FT_STATUS s;
	FT_HANDLE h;

	if (!PyArg_ParseTuple(ao, "i:open", &dn)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_Open(dn, &h);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		// PyErr_SetObject(Fterr, PyInt_FromLong(s));
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}
	else return (PyObject*)ftobj_alloc(h);
}

//    PVOID pArg1,
//    DWORD Flags,
//    FT_HANDLE *pHandle
static PyObject*
ftobj_OpenEx(Ftobj *fo, PyObject *ao) {
	int r = 0;
	FT_STATUS s;
	FT_HANDLE h;
	char *n;

	if (!PyArg_ParseTuple(ao, "s|i:openEx", &n, &r)) return NULL;
	if (r == 0) r = FT_OPEN_BY_SERIAL_NUMBER; // default

	Py_BEGIN_ALLOW_THREADS
	s = FT_OpenEx(n, (DWORD)r, &h);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		// PyErr_SetObject(Fterr, PyInt_FromLong(s));
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}
	else return (PyObject*)ftobj_alloc(h);
}

//  FT_HANDLE ftHandle
static PyObject*
ftobj_Close(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;

	if (!PyArg_ParseTuple(ao, ":close")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_Close(fo->handle);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	fo->status &= ~Ftobj_OPEN; // clear open bit
	Py_INCREF(Py_None); return Py_None;
}

//  FT_HANDLE ftHandle,
//  LPVOID lpBuffer,
//  DWORD nBufferSize,
//  LPDWORD lpBytesReturned
static PyObject*
ftobj_Read(Ftobj *fo, PyObject *ao) {
	PyObject *result = NULL;
	FT_STATUS s;
	int l; // bytes to read, bytes returned
	DWORD r;
	char *b;

	if (!PyArg_ParseTuple(ao, "i:read", &l)) return NULL;
	b = (char *)PyMem_Malloc(l*sizeof(char));
	if (!b) return PyErr_NoMemory();

	Py_BEGIN_ALLOW_THREADS
	s = FT_Read(fo->handle, b, l, &r);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		goto end;
	}

	result = PyString_FromStringAndSize(b, r);

end:
	PyMem_Free(b);
	return result;
}

//    FT_HANDLE ftHandle,
//    LPVOID lpBuffer,
//    DWORD nBufferSize,
//    LPDWORD lpBytesWritten
static PyObject*
ftobj_Write(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	char *b;
	int l;
	DWORD w;

	if (!PyArg_ParseTuple(ao, "s#:write", &b, &l)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_Write(fo->handle, b, l, &w);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong((int)w);
}

//    FT_HANDLE ftHandle,
//    DWORD dwIoControlCode,
//    LPVOID lpInBuf,
//    DWORD nInBufSize,
//    LPVOID lpOutBuf,
//    DWORD nOutBufSize,
//    LPDWORD lpBytesReturned,
//    LPOVERLAPPED lpOverlapped
static PyObject*
ftobj_IoCtl(Ftobj *fo, PyObject *ao) {
	return NULL;
}

//  FT_HANDLE ftHandle,
//	ULONG BaudRate
static PyObject*
ftobj_SetBaudRate(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	int b;

	if (!PyArg_ParseTuple(ao, "i:setBaudRate", &b)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetBaudRate(fo->handle, (DWORD)b);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//  FT_HANDLE ftHandle,
//	USHORT Divisor
static PyObject*
ftobj_SetDivisor(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	int b;

	if (!PyArg_ParseTuple(ao, "i:setDivisor", &b)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetDivisor(fo->handle, (USHORT)b);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//	UCHAR WordLength,
//	UCHAR StopBits,
//	UCHAR Parity
static PyObject*
ftobj_SetDataCharacteristics(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	int l, b, p; // length, stop bits, parity

	if (!PyArg_ParseTuple(ao, "iii:setDataCharacteristics", &l, &b, &p)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetDataCharacteristics(fo->handle, (UCHAR)l, (UCHAR)b, (UCHAR)p);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//    USHORT FlowControl,
//    UCHAR XonChar,
//    UCHAR XoffChar
static PyObject*
ftobj_SetFlowControl(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	int c, o = -1, f = -1; // flow, xon xoff

	if (!PyArg_ParseTuple(ao, "i|ii:setFlowControl", &c, &o, &f)) return NULL;
	if (c == FT_FLOW_XON_XOFF && (o == -1 || f == -1)) {
		PyErr_SetNone(PyExc_TypeError);
		return NULL;
	}

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetFlowControl(fo->handle, (USHORT)c, (UCHAR)o, (UCHAR)f);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle
static PyObject*
ftobj_ResetDevice(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;

	if (!PyArg_ParseTuple(ao, ":resetDevice")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_ResetDevice(fo->handle);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle
static PyObject*
ftobj_SetDtr(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;

	if (!PyArg_ParseTuple(ao, ":setDtr")) return NULL;
	Py_BEGIN_ALLOW_THREADS
	s = FT_SetDtr(fo->handle);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle
static PyObject*
ftobj_ClrDtr(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;

	if (!PyArg_ParseTuple(ao, ":clrDtr")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_ClrDtr(fo->handle);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle
static PyObject*
ftobj_SetRts(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;

	if (!PyArg_ParseTuple(ao, ":setRts")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetRts(fo->handle);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle
static PyObject*
ftobj_ClrRts(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;

	if (!PyArg_ParseTuple(ao, ":clrRts")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_ClrRts(fo->handle);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//	ULONG *pModemStatus
static PyObject*
ftobj_GetModemStatus(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	ULONG m;

	if (!PyArg_ParseTuple(ao, ":getModemStatus")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_GetModemStatus(fo->handle, &m);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong((int)m);
}

//    FT_HANDLE ftHandle,
//	UCHAR EventChar,
//	UCHAR EventCharEnabled,
//	UCHAR ErrorChar,
//	UCHAR ErrorCharEnabled
static PyObject*
ftobj_SetChars(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	char ev, er;
	int eve, ere;

	if (!PyArg_ParseTuple(ao, "cici:setChars", &ev, &eve, &er, &ere)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetChars(fo->handle, (UCHAR)ev, (UCHAR)eve, (UCHAR)er, (UCHAR)ere);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//	ULONG Mask
static PyObject*
ftobj_Purge(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	int m = 0;

	if (!PyArg_ParseTuple(ao, "|i:purge", &m)) return NULL;
	if (m == 0) m = FT_PURGE_RX | FT_PURGE_TX;

	Py_BEGIN_ALLOW_THREADS
	s = FT_Purge(fo->handle, (DWORD)m);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//	ULONG ReadTimeout,
//	ULONG WriteTimeout
static PyObject*
ftobj_SetTimeouts(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	int r, w;

	if (!PyArg_ParseTuple(ao, "ii:setTimeouts", &r, &w)) return NULL;
	// if (PyTuple_Size(ao) == 2) w = r; // equal timeouts

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetTimeouts(fo->handle, (DWORD)r, (DWORD)w);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//  FT_HANDLE ftHandle,
//	ULONG DeadmanTimeout
static PyObject*
ftobj_SetDeadmanTimeout(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	DWORD d;

	if (!PyArg_ParseTuple(ao, "i:setDeadmanTimeout", &d)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetDeadmanTimeout(fo->handle, d);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//	DWORD *dwRxBytes
static PyObject*
ftobj_GetQueueStatus(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	DWORD r;

	if (!PyArg_ParseTuple(ao, ":getQueueStatus")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_GetQueueStatus(fo->handle, &r);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong((int)r);
}

//  FT_HANDLE ftHandle,
//	DWORD Mask,
//	PVOID Param
static PyObject*
ftobj_SetEventNotification(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	HANDLE e; // PyHANDLE event handle
	int m;

	if (!PyArg_ParseTuple(ao, "ii:setEventNotification", &m, &e)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetEventNotification(fo->handle, (DWORD)m, (HANDLE)e);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//    DWORD *dwRxBytes,
//    DWORD *dwTxBytes,
//    DWORD *dwEventDWord
static PyObject*
ftobj_GetStatus(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	DWORD r, t, e;

	if (!PyArg_ParseTuple(ao, ":getStatus")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_GetStatus(fo->handle, &r, &t, &e);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return Py_BuildValue("(iii)", (int)r, (int)t, (int)e);
}

//    FT_HANDLE ftHandle
static PyObject*
ftobj_SetBreakOn(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;

	if (!PyArg_ParseTuple(ao, ":setBreakOn")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetBreakOn(fo->handle);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle
static PyObject*
ftobj_SetBreakOff(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;

	if (!PyArg_ParseTuple(ao, ":setBreakOff")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetBreakOff(fo->handle);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//    DWORD Mask
static PyObject*
ftobj_SetWaitMask(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	int m;

	if (!PyArg_ParseTuple(ao, "i:setWaitMask", &m)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetWaitMask(fo->handle, (DWORD)m);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//    DWORD *Mask
static PyObject*
ftobj_WaitOnMask(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	DWORD m;

	if (!PyArg_ParseTuple(ao, ":waitOnMask")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_WaitOnMask(fo->handle, &m);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong((int)m);
}

//    FT_HANDLE ftHandle,
//    DWORD *dwEventDWord
static PyObject*
ftobj_GetEventStatus(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	DWORD m;

	if (!PyArg_ParseTuple(ao, ":getEventStatus")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_GetEventStatus(fo->handle, &m);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong((int)m);
}

//    FT_HANDLE ftHandle,
//    UCHAR ucLatency
static PyObject*
ftobj_SetLatencyTimer(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	int m;

	if (!PyArg_ParseTuple(ao, "i:setLatencyTimer", &m)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetLatencyTimer(fo->handle, (UCHAR)m);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//    PUCHAR pucLatency
static PyObject*
ftobj_GetLatencyTimer(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	UCHAR m;

	if (!PyArg_ParseTuple(ao, ":getLatencyTimer")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_GetLatencyTimer(fo->handle, &m);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong((int)m);
}

/*
ucMask Required value for bit mode mask. This sets up which bits are
inputs and outputs. A bit value of 0 sets the corresponding pin
to an input, a bit value of 1 sets the corresponding pin to an
output.
In the case of CBUS Bit Bang, the upper nibble of this value
controls which pins are inputs and outputs, while the lower
nibble controls which of the outputs are high and low.
ucMode Mode value. Can be one of the following:
0x0 = Reset
0x1 = Asynchronous Bit Bang
0x2 = MPSSE (FT2232C devices only)
0x4 = Synchronous Bit Bang (FT232R, FT245R and FT2232C
devices only)
0x8 = MCU Host Bus Emulation Mode (FT2232C devices only)
0x10 = Fast Opto-Isolated Serial Mode (FT2232C devices only)
0x20 = CBUS Bit Bang Mode (FT232R devices only)
*/
//    FT_HANDLE ftHandle,
//    UCHAR ucMask,
//	UCHAR ucEnable
static PyObject*
ftobj_SetBitMode(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	int m, e;

	if (!PyArg_ParseTuple(ao, "ii:setBitMode", &m, &e)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetBitMode(fo->handle, (UCHAR)m, (UCHAR)e);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//    PUCHAR pucMode
static PyObject*
ftobj_GetBitMode(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	UCHAR m;

	if (!PyArg_ParseTuple(ao, ":getBitMode")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_GetBitMode(fo->handle, &m);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong((int)m);
}

//    FT_HANDLE ftHandle,
//    ULONG ulInTransferSize,
//    ULONG ulOutTransferSize
static PyObject*
ftobj_SetUSBParameters(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	int i, o = 0; // out transfer zero when not supported

	if (!PyArg_ParseTuple(ao, "i|i:setBitMode", &i, &o)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetBitMode(fo->handle, (ULONG)i, (ULONG)o);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//    FT_DEVICE *lpftDevice,
//	LPDWORD lpdwID,
//	PCHAR SerialNumber,
//	PCHAR Description,
//	LPVOID Dummy
static PyObject*
ftobj_GetDeviceInfo(Ftobj *fo, PyObject *ao) {
	PyObject *result = NULL;
	FT_STATUS s;

	DWORD deviceType, deviceId;
	char description[256];
	char serial[256];

	if (!PyArg_ParseTuple(ao, ":getDeviceInfo")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_GetDeviceInfo(fo->handle, (FT_DEVICE*)&deviceType, (DWORD*)&deviceId, serial, description, NULL);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	result = PyDict_New();
	if (!result) return PyErr_NoMemory();

	PyDict_SetItemString(result, "type", PyInt_FromLong((int)deviceType));
	PyDict_SetItemString(result, "id", PyInt_FromLong((int)deviceId));
	PyDict_SetItemString(result, "description", PyString_FromString(description));
	PyDict_SetItemString(result, "serial", PyString_FromString(serial));

	return result;
}

//    FT_HANDLE ftHandle
static PyObject*
ftobj_StopInTask(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;

	if (!PyArg_ParseTuple(ao, ":stopInTask")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_StopInTask(fo->handle);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle
static PyObject*
ftobj_RestartInTask(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;

	if (!PyArg_ParseTuple(ao, ":restartInTask")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_RestartInTask(fo->handle);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//	DWORD dwCount
static PyObject*
ftobj_SetResetPipeRetryCount(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	int c;

	if (!PyArg_ParseTuple(ao, "i:setResetPipeRetryCount", &c)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_SetResetPipeRetryCount(fo->handle, (DWORD)c);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle
static PyObject*
ftobj_ResetPort(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;

	if (!PyArg_ParseTuple(ao, ":resetPort")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_ResetPort(fo->handle);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle
static PyObject*
ftobj_CyclePort(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;

	if (!PyArg_ParseTuple(ao, ":cyclePort")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_CyclePort(fo->handle);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//	LPDWORD lpdwDriverVersion
static PyObject*
ftobj_GetDriverVersion(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	DWORD m;

	if (!PyArg_ParseTuple(ao, ":getDriverVersion")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_GetDriverVersion(fo->handle, &m);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong((int)m);
}

//    FT_HANDLE ftHandle,
//	LPLONG lpdwComPortNumber
static PyObject*
ftobj_GetComPortNumber(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	LONG m;

	if (!PyArg_ParseTuple(ao, ":getComPortNumber")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_GetComPortNumber(fo->handle, &m);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong(m);
}

static PyObject*
ftobj_EE_ReadConfig(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	int a;
	UCHAR v;

	if (!PyArg_ParseTuple(ao, "i:eeReadConfig", &a)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_EE_ReadConfig(fo->handle, (UCHAR)a, &v);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong(v);
}

static PyObject*
ftobj_EE_WriteConfig(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	int a, v;

	if (!PyArg_ParseTuple(ao, "ii:eeWriteConfig", &a, &v)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_EE_WriteConfig(fo->handle, (UCHAR)a, (UCHAR)v);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

static PyObject*
ftobj_EE_ReadEcc(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	int o;
	WORD v;

	if (!PyArg_ParseTuple(ao, "i:eeReadEcc", &o)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_EE_ReadEcc(fo->handle, (UCHAR)o, &v);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong(v);
}

static PyObject*
ftobj_GetQueueStatusEx(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	DWORD r;

	if (!PyArg_ParseTuple(ao, ":getQueueStatusEx")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_GetQueueStatusEx(fo->handle, &r);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong((int)r);
}

//typedef struct ft_program_data {
//	DWORD Signature1;			// Header - must be 0x00000000
//	DWORD Signature2;			// Header - must be 0xffffffff
//	DWORD Version;				// Header - FT_PROGRAM_DATA version
//								//          0 = original
//                //          1 = FT2232C extensions
//                //			3 = FT2232H extensions
//                //			4 = FT4232H extensions
//                //			5 = FT232H extensions
//	WORD VendorId;				// 0x0403
//	WORD ProductId;				// 0x6001
//	char *Manufacturer;			// "FTDI"
//	char *ManufacturerId;		// "FT"
//	char *Description;			// "USB HS Serial Converter"
//	char *SerialNumber;			// "FT000001" if fixed, or NULL
//	WORD MaxPower;				// 0 < MaxPower <= 500
//	WORD PnP;					// 0 = disabled, 1 = enabled
//	WORD SelfPowered;			// 0 = bus powered, 1 = self powered
//	WORD RemoteWakeup;			// 0 = not capable, 1 = capable
//	//
//	// Rev4 extensions
//	//
//	UCHAR Rev4;					// non-zero if Rev4 chip, zero otherwise
//	UCHAR IsoIn;				// non-zero if in endpoint is isochronous
//	UCHAR IsoOut;				// non-zero if out endpoint is isochronous
//	UCHAR PullDownEnable;		// non-zero if pull down enabled
//	UCHAR SerNumEnable;			// non-zero if serial number to be used
//	UCHAR USBVersionEnable;		// non-zero if chip uses USBVersion
//	WORD USBVersion;			// BCD (0x0200 => USB2)
//	//
//	// FT2232C extensions
//	//
//	UCHAR Rev5;					// non-zero if Rev5 chip, zero otherwise
//	UCHAR IsoInA;				// non-zero if in endpoint is isochronous
//	UCHAR IsoInB;				// non-zero if in endpoint is isochronous
//	UCHAR IsoOutA;				// non-zero if out endpoint is isochronous
//	UCHAR IsoOutB;				// non-zero if out endpoint is isochronous
//	UCHAR PullDownEnable5;		// non-zero if pull down enabled
//	UCHAR SerNumEnable5;		// non-zero if serial number to be used
//	UCHAR USBVersionEnable5;	// non-zero if chip uses USBVersion
//	WORD USBVersion5;			// BCD (0x0200 => USB2)
//	UCHAR AIsHighCurrent;		// non-zero if interface is high current
//	UCHAR BIsHighCurrent;		// non-zero if interface is high current
//	UCHAR IFAIsFifo;			// non-zero if interface is 245 FIFO
//	UCHAR IFAIsFifoTar;			// non-zero if interface is 245 FIFO CPU target
//	UCHAR IFAIsFastSer;			// non-zero if interface is Fast serial
//	UCHAR AIsVCP;				// non-zero if interface is to use VCP drivers
//	UCHAR IFBIsFifo;			// non-zero if interface is 245 FIFO
//	UCHAR IFBIsFifoTar;			// non-zero if interface is 245 FIFO CPU target
//	UCHAR IFBIsFastSer;			// non-zero if interface is Fast serial
//	UCHAR BIsVCP;				// non-zero if interface is to use VCP drivers
//	//
//	// FT232R extensions
//	//
//	UCHAR UseExtOsc;			// Use External Oscillator
//	UCHAR HighDriveIOs;			// High Drive I/Os
//	UCHAR EndpointSize;			// Endpoint size
//
//	UCHAR PullDownEnableR;		// non-zero if pull down enabled
//	UCHAR SerNumEnableR;		// non-zero if serial number to be used
//
//	UCHAR InvertTXD;			// non-zero if invert TXD
//	UCHAR InvertRXD;			// non-zero if invert RXD
//	UCHAR InvertRTS;			// non-zero if invert RTS
//	UCHAR InvertCTS;			// non-zero if invert CTS
//	UCHAR InvertDTR;			// non-zero if invert DTR
//	UCHAR InvertDSR;			// non-zero if invert DSR
//	UCHAR InvertDCD;			// non-zero if invert DCD
//	UCHAR InvertRI;				// non-zero if invert RI
//
//	UCHAR Cbus0;				// Cbus Mux control
//	UCHAR Cbus1;				// Cbus Mux control
//	UCHAR Cbus2;				// Cbus Mux control
//	UCHAR Cbus3;				// Cbus Mux control
//	UCHAR Cbus4;				// Cbus Mux control
//
//	UCHAR RIsD2XX;				// non-zero if using VCP drivers

	////
	//// Rev 7 (FT2232H) Extensions
	////
	//UCHAR PullDownEnable7;		// non-zero if pull down enabled
	//UCHAR SerNumEnable7;		// non-zero if serial number to be used
	//UCHAR ALSlowSlew;			// non-zero if AL pins have slow slew
	//UCHAR ALSchmittInput;		// non-zero if AL pins are Schmitt input
	//UCHAR ALDriveCurrent;		// valid values are 4mA, 8mA, 12mA, 16mA
	//UCHAR AHSlowSlew;			// non-zero if AH pins have slow slew
	//UCHAR AHSchmittInput;		// non-zero if AH pins are Schmitt input
	//UCHAR AHDriveCurrent;		// valid values are 4mA, 8mA, 12mA, 16mA
	//UCHAR BLSlowSlew;			// non-zero if BL pins have slow slew
	//UCHAR BLSchmittInput;		// non-zero if BL pins are Schmitt input
	//UCHAR BLDriveCurrent;		// valid values are 4mA, 8mA, 12mA, 16mA
	//UCHAR BHSlowSlew;			// non-zero if BH pins have slow slew
	//UCHAR BHSchmittInput;		// non-zero if BH pins are Schmitt input
	//UCHAR BHDriveCurrent;		// valid values are 4mA, 8mA, 12mA, 16mA
	//UCHAR IFAIsFifo7;			// non-zero if interface is 245 FIFO
	//UCHAR IFAIsFifoTar7;		// non-zero if interface is 245 FIFO CPU target
	//UCHAR IFAIsFastSer7;		// non-zero if interface is Fast serial
	//UCHAR AIsVCP7;				// non-zero if interface is to use VCP drivers
	//UCHAR IFBIsFifo7;			// non-zero if interface is 245 FIFO
	//UCHAR IFBIsFifoTar7;		// non-zero if interface is 245 FIFO CPU target
	//UCHAR IFBIsFastSer7;		// non-zero if interface is Fast serial
	//UCHAR BIsVCP7;				// non-zero if interface is to use VCP drivers
	//UCHAR PowerSaveEnable;		// non-zero if using BCBUS7 to save power for self-powered designs
	////
	//// Rev 8 (FT4232H) Extensions
	////
	//UCHAR PullDownEnable8;		// non-zero if pull down enabled
	//UCHAR SerNumEnable8;		// non-zero if serial number to be used
	//UCHAR ASlowSlew;			// non-zero if AL pins have slow slew
	//UCHAR ASchmittInput;		// non-zero if AL pins are Schmitt input
	//UCHAR ADriveCurrent;		// valid values are 4mA, 8mA, 12mA, 16mA
	//UCHAR BSlowSlew;			// non-zero if AH pins have slow slew
	//UCHAR BSchmittInput;		// non-zero if AH pins are Schmitt input
	//UCHAR BDriveCurrent;		// valid values are 4mA, 8mA, 12mA, 16mA
	//UCHAR CSlowSlew;			// non-zero if BL pins have slow slew
	//UCHAR CSchmittInput;		// non-zero if BL pins are Schmitt input
	//UCHAR CDriveCurrent;		// valid values are 4mA, 8mA, 12mA, 16mA
	//UCHAR DSlowSlew;			// non-zero if BH pins have slow slew
	//UCHAR DSchmittInput;		// non-zero if BH pins are Schmitt input
	//UCHAR DDriveCurrent;		// valid values are 4mA, 8mA, 12mA, 16mA
	//UCHAR ARIIsTXDEN;			// non-zero if port A uses RI as RS485 TXDEN
	//UCHAR BRIIsTXDEN;			// non-zero if port B uses RI as RS485 TXDEN
	//UCHAR CRIIsTXDEN;			// non-zero if port C uses RI as RS485 TXDEN
	//UCHAR DRIIsTXDEN;			// non-zero if port D uses RI as RS485 TXDEN
	//UCHAR AIsVCP8;				// non-zero if interface is to use VCP drivers
	//UCHAR BIsVCP8;				// non-zero if interface is to use VCP drivers
	//UCHAR CIsVCP8;				// non-zero if interface is to use VCP drivers
	//UCHAR DIsVCP8;				// non-zero if interface is to use VCP drivers



//} FT_PROGRAM_DATA, *PFT_PROGRAM_DATA;

// { 0x0403, 0x6001, "FTDI", "FT", "USB HS Serial Converter", "", 44, 1, 0, 1,
// FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, 0 }

//    FT_HANDLE ftHandle,
//	PFT_PROGRAM_DATA pData
static PyObject*
ftobj_EE_Program(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	FT_PROGRAM_DATA d;
	PyObject *ad, *k, *v;
	int p = 0;

	static char *kw[] = {
		"manufacturer",
		"manufacturerId",
		"description",
		"serialNumber",

		"signature1",
		"signature2",
		"version",

		"vendorId",
		"productId",
		"maxPower",
		"pnp",
		"selfPowered",
		"remoteWakeup",

		"rev4",
		"isoIn",
		"isoOut",
		"pullDownEnable",
		"serNumEnable",
		"usbVersionEnable",
		"usbVersion",

		"rev5",
		"isoInA",
		"isoInB",
		"isoOutA",
		"isoOutB",
		"pullDownEnable5",
		"serNumEnable5",
		"usbVersionEnable5",
		"usbVersion5",
		"aIsHighCurrent",
		"bIsHighCurrent",
		"ifAIsFifo",
		"ifAIsFifoTar",
		"ifAIsFastSer",
		"aIsVCP",
		"ifBIsFifo",
		"ifBIsFifoTar",
		"ifBIsFastSer",
		"bIsVCP",

		"useExtOsc",
		"highDriveIOs",
		"endpointSize",
		"pullDownEnableR",
		"serNumEnableR",
		"invertTXD",
		"invertRXD",
		"invertRTS",
		"invertCTS",
		"invertDTR",
		"invertDSR",
		"invertDCD",
		"invertRI",
		"cbus0",
		"cbus1",
		"cbus2",
		"cbus3",
		"cbus4",
		"rIsD2XX",

		"pullDownEnable7",
		"serNumEnable7",
		"alSlowSlew",
		"alSchmittInput",
		"alDriveCurrent",
		"ahSlowSlew",
		"ahSchmittInput",
		"ahDriveCurrent",
		"blSlowSlew",
		"blSchmittInput",
		"blDriveCurrent",
		"bhSlowSlew",
		"bhSchmittInput",
		"bhDriveCurrent",
		"ifAIsFifo7",
		"ifAIsFifoTar7",
		"ifAIsFastSer7",
		"aIsVCP7",
		"ifBIsFifo7",
		"ifBIsFifoTar7",
		"ifBIsFastSer7",
		"bIsVCP7",
		"powerSaveEnable",

		"pullDownEnable8",
		"serNumEnable8",
		"aSlowSlew",
		"aSchmittInput",
		"aDriveCurrent",
		"bSlowSlew",
		"bSchmittInput",
		"bDriveCurrent",
		"cSlowSlew",
		"cSchmittInput",
		"cDriveCurrent",
		"dSlowSlew",
		"dSchmittInput",
		"dDriveCurrent",
		"aRIIsTXDEN",
		"bRIIsTXDEN",
		"cRIIsTXDEN",
		"dRIIsTXDEN",
		"aIsVCP8",
		"bIsVCP8",
		"cIsVCP8",
		"dIsVCP8",

		"pullDownEnableH",
		"serNumEnableH",
		"acSlowSlewH",
		"acSchmittInputH",
		"acDriveCurrentH",
		"adSlowSlewH",
		"adSchmittInputH",
		"adDriveCurrentH",
		"cbus0H",
		"cbus1H",
		"cbus2H",
		"cbus3H",
		"cbus4H",
		"cbus5H",
		"cbus6H",
		"cbus7H",
		"cbus8H",
		"cbus9H",
		"isFifoH",
		"isFifoTarH",
		"isFastSerH",
		"isFt1248H",
		"ft1248CpolH",
		"ft1248LsbH",
		"ft1248FlowControlH",
		"isVCPH",
		"powerSaveEnableH",
		NULL
	};

	if (!PyArg_ParseTuple(ao, "O:eeProgram", &ad)) return NULL;

	memset(&d, 0, sizeof(d));

	d.Signature1 = 0x00000000;
	d.Signature2 = 0xffffffff;
	d.Version = -1; // force error if not set

	d.VendorId = 0; //0x0403;
	d.ProductId = 0; //0x6001;
	d.Manufacturer = NULL; // "FTDI";
	d.ManufacturerId = NULL; // "FT";
	d.Description = NULL; // "USB HS Serial Converter";
	d.SerialNumber = NULL; //""; // "FT000001"; // if fixed, or NULL
	d.MaxPower = 44; // 0 < MaxPower <= 500
	d.PnP = 1; // 0 = disabled, 1 = enabled
	d.SelfPowered = 0; // 0 = bus powered, 1 = self powered
	d.RemoteWakeup = 1; // 0 = not capable, 1 = capable

	s = FT_OK;
	if (!PyDict_Check(ad)) s = FT_INVALID_ARGS;
	else while (PyDict_Next(ad, &p, &k, &v)) {
		if (!PyString_Check(k)) {
			s = FT_INVALID_ARGS;
			break;
		}
		else {
			char *ks = PyString_AsString(k), *vs;
			int i, l = sizeof(kw)/sizeof(char *), vi;
			for (i=0; i<l; ++i) if (!strcmp(kw[i], ks)) break;
			if (i == l) continue; // key not found, ignore
			if (i < 4) { // parse string
				if (!PyString_Check(v)) {
					s = FT_INVALID_PARAMETER;
					break;
				}
				else {
					vs = PyString_AsString(v);
					switch (i) {
					case 0: d.Manufacturer = vs; break;
					case 1: d.ManufacturerId = vs; break;
					case 2: d.Description = vs; break;
					case 3: d.SerialNumber = vs; break;
					default: break;
					}
				}

			}
			else { // parse int
				if (!PyInt_Check(v)) {
					s = FT_INVALID_PARAMETER;
					break;
				}
				else {
					vi = PyInt_AsLong(v);
					switch (i) {
					case 4: d.Signature1 = vi; break;
					case 5: d.Signature2 = vi; break;
					case 6: d.Version = vi; break;
					case 7: d.VendorId = vi; break;
					case 8: d.ProductId = vi; break;
					case 9: d.MaxPower = vi; break;
					case 10: d.PnP = vi; break;
					case 11: d.SelfPowered = vi; break;
					case 12: d.RemoteWakeup = vi; break;
					case 13: d.Rev4 = vi; break;
					case 14: d.IsoIn = vi; break;
					case 15: d.IsoOut = vi; break;
					case 16: d.PullDownEnable = vi; break;
					case 17: d.SerNumEnable = vi; break;
					case 18: d.USBVersionEnable = vi; break;
					case 19: d.USBVersion = vi; break;
					case 20: d.Rev5 = vi; break;
					case 21: d.IsoInA = vi; break;
					case 22: d.IsoInB = vi; break;
					case 23: d.IsoOutA = vi; break;
					case 24: d.IsoOutB = vi; break;
					case 25: d.PullDownEnable5 = vi; break;
					case 26: d.SerNumEnable5 = vi; break;
					case 27: d.USBVersionEnable5 = vi; break;
					case 28: d.USBVersion5 = vi; break;
					case 29: d.AIsHighCurrent = vi; break;
					case 30: d.BIsHighCurrent = vi; break;
					case 31: d.IFAIsFifo = vi; break;
					case 32: d.IFAIsFifoTar = vi; break;
					case 33: d.IFAIsFastSer = vi; break;
					case 34: d.AIsVCP = vi; break;
					case 35: d.IFBIsFifo = vi; break;
					case 36: d.IFBIsFifoTar = vi; break;
					case 37: d.IFBIsFastSer = vi; break;
					case 38: d.BIsVCP = vi; break;
					case 39: d.UseExtOsc = vi; break;
					case 40: d.HighDriveIOs = vi; break;
					case 41: d.EndpointSize = vi; break;
					case 42: d.PullDownEnableR = vi; break;
					case 43: d.SerNumEnableR = vi; break;
					case 44: d.InvertTXD = vi; break;
					case 45: d.InvertRXD = vi; break;
					case 46: d.InvertRTS = vi; break;
					case 47: d.InvertCTS = vi; break;
					case 48: d.InvertDTR = vi; break;
					case 49: d.InvertDSR = vi; break;
					case 50: d.InvertDCD = vi; break;
					case 51: d.InvertRI = vi; break;
					case 52: d.Cbus0 = vi; break;
					case 53: d.Cbus1 = vi; break;
					case 54: d.Cbus2 = vi; break;
					case 55: d.Cbus3 = vi; break;
					case 56: d.Cbus4 = vi; break;
					case 57: d.RIsD2XX = vi; break;

					case 58: d.PullDownEnable7 = vi; break;
					case 59: d.SerNumEnable7 = vi; break;
					case 60: d.ALSlowSlew = vi; break;
					case 61: d.ALSchmittInput = vi; break;
					case 62: d.ALDriveCurrent = vi; break;
					case 63: d.AHSlowSlew = vi; break;
					case 64: d.AHSchmittInput = vi; break;
					case 65: d.AHDriveCurrent = vi; break;
					case 66: d.BLSlowSlew = vi; break;
					case 67: d.BLSchmittInput = vi; break;
					case 68: d.BLDriveCurrent = vi; break;
					case 69: d.BHSlowSlew = vi; break;
					case 70: d.BHSchmittInput = vi; break;
					case 71: d.BHDriveCurrent = vi; break;
					case 72: d.IFAIsFifo7 = vi; break;
					case 73: d.IFAIsFifoTar7 = vi; break;
					case 74: d.IFAIsFastSer7 = vi; break;
					case 75: d.AIsVCP7 = vi; break;
					case 76: d.IFBIsFifo7 = vi; break;
					case 77: d.IFBIsFifoTar7 = vi; break;
					case 78: d.IFBIsFastSer7 = vi; break;
					case 79: d.BIsVCP7 = vi; break;
					case 80: d.PowerSaveEnable = vi; break;

					case 81: d.PullDownEnable8 = vi; break;
					case 82: d.SerNumEnable8 = vi; break;
					case 83: d.ASlowSlew = vi; break;
					case 84: d.ASchmittInput = vi; break;
					case 85: d.ADriveCurrent = vi; break;
					case 86: d.BSlowSlew = vi; break;
					case 87: d.BSchmittInput = vi; break;
					case 88: d.BDriveCurrent = vi; break;
					case 89: d.CSlowSlew = vi; break;
					case 90: d.CSchmittInput = vi; break;
					case 91: d.CDriveCurrent = vi; break;
					case 92: d.DSlowSlew = vi; break;
					case 93: d.DSchmittInput = vi; break;
					case 94: d.DDriveCurrent = vi; break;
					case 95: d.ARIIsTXDEN = vi; break;
					case 96: d.BRIIsTXDEN = vi; break;
					case 97: d.CRIIsTXDEN = vi; break;
					case 98: d.DRIIsTXDEN = vi; break;
					case 99: d.AIsVCP8 = vi; break;
					case 100: d.BIsVCP8 = vi; break;
					case 101: d.CIsVCP8 = vi; break;
					case 102: d.DIsVCP8 = vi; break;

					case 103: d.PullDownEnableH = vi; break;
					case 104: d.SerNumEnableH = vi; break;
					case 105: d.ACSlowSlewH = vi; break;
					case 106: d.ACSchmittInputH = vi; break;
					case 107: d.ACDriveCurrentH = vi; break;
					case 108: d.ADSlowSlewH = vi; break;
					case 109: d.ADSchmittInputH = vi; break;
					case 110: d.ADDriveCurrentH = vi; break;
					case 111: d.Cbus0H = vi; break;
					case 112: d.Cbus1H = vi; break;
					case 113: d.Cbus2H = vi; break;
					case 114: d.Cbus3H = vi; break;
					case 115: d.Cbus4H = vi; break;
					case 116: d.Cbus5H = vi; break;
					case 117: d.Cbus6H = vi; break;
					case 118: d.Cbus7H = vi; break;
					case 119: d.Cbus8H = vi; break;
					case 120: d.Cbus9H = vi; break;
					case 121: d.IsFifoH = vi; break;
					case 122: d.IsFifoTarH = vi; break;
					case 123: d.IsFastSerH = vi; break;
					case 124: d.IsFT1248H = vi; break;
					case 125: d.FT1248CpolH = vi; break;
					case 126: d.FT1248LsbH = vi; break;
					case 127: d.FT1248FlowControlH = vi; break;
					case 128: d.IsVCPH = vi; break;
					case 129: d.PowerSaveEnableH = vi; break;

					default: break;
					}
				}
			}
		}
	}

	if (s != FT_OK) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_BEGIN_ALLOW_THREADS
	s = FT_EE_Program(fo->handle, &d);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//	PFT_PROGRAM_DATA pData
static PyObject*
ftobj_EE_Read(Ftobj *fo, PyObject *ao) {
	PyObject *result = NULL;
	FT_STATUS s;
	FT_PROGRAM_DATA d;
	char manufacturer[256];
	char manufacturerId[256];
	char description[256];
	char serialNumber[256];

	d.Signature1 = 0x00000000;
	d.Signature2 = 0xffffffff;
	d.Version = 2;

	d.Manufacturer = manufacturer;
	d.ManufacturerId = manufacturerId;
	d.Description = description;
	d.SerialNumber = serialNumber;

	if (!PyArg_ParseTuple(ao, ":eeRead")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_EE_Read(fo->handle, &d);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	result = PyDict_New();
	if (!result) return PyErr_NoMemory();

	PyDict_SetItemString(result, "signature1", PyInt_FromLong((int)d.Signature1));
	PyDict_SetItemString(result, "signature2", PyInt_FromLong((int)d.Signature2));
	PyDict_SetItemString(result, "version", PyInt_FromLong((int)d.Version));

	PyDict_SetItemString(result, "vendorId", PyInt_FromLong((int)d.VendorId));
	PyDict_SetItemString(result, "productId", PyInt_FromLong((int)d.ProductId));
	PyDict_SetItemString(result, "manufacturer", PyString_FromString(manufacturer));
	PyDict_SetItemString(result, "manufacturerId", PyString_FromString(manufacturerId));
	PyDict_SetItemString(result, "description", PyString_FromString(description));
	PyDict_SetItemString(result, "serialNumber", PyString_FromString(serialNumber));
	PyDict_SetItemString(result, "maxPower", PyInt_FromLong((int)d.MaxPower));
	PyDict_SetItemString(result, "pnp", PyInt_FromLong((int)d.PnP));
	PyDict_SetItemString(result, "selfPowered", PyInt_FromLong((int)d.SelfPowered));
	PyDict_SetItemString(result, "remoteWakeup", PyInt_FromLong((int)d.RemoteWakeup));

	PyDict_SetItemString(result, "rev4", PyInt_FromLong((int)d.Rev4));
	PyDict_SetItemString(result, "isoIn", PyInt_FromLong((int)d.IsoIn));
	PyDict_SetItemString(result, "isoOut", PyInt_FromLong((int)d.IsoOut));
	PyDict_SetItemString(result, "pullDownEnable", PyInt_FromLong((int)d.PullDownEnable));
	PyDict_SetItemString(result, "serNumEnable", PyInt_FromLong((int)d.SerNumEnable));
	PyDict_SetItemString(result, "usbVersionEnable", PyInt_FromLong((int)d.USBVersionEnable));
	PyDict_SetItemString(result, "usbVersion", PyInt_FromLong((int)d.USBVersion));

	PyDict_SetItemString(result, "rev5", PyInt_FromLong((int)d.Rev5));
	PyDict_SetItemString(result, "isoInA", PyInt_FromLong((int)d.IsoInA));
	PyDict_SetItemString(result, "isoInB", PyInt_FromLong((int)d.IsoInB));
	PyDict_SetItemString(result, "isoOutA", PyInt_FromLong((int)d.IsoOutA));
	PyDict_SetItemString(result, "isoOutB", PyInt_FromLong((int)d.IsoOutB));
	PyDict_SetItemString(result, "pullDownEnable5", PyInt_FromLong((int)d.PullDownEnable5));
	PyDict_SetItemString(result, "serNumEnable5", PyInt_FromLong((int)d.SerNumEnable5));
	PyDict_SetItemString(result, "usbVersionEnable5", PyInt_FromLong((int)d.USBVersionEnable5));
	PyDict_SetItemString(result, "usbVersion5", PyInt_FromLong((int)d.USBVersion5));
	PyDict_SetItemString(result, "aIsHighCurrent", PyInt_FromLong((int)d.AIsHighCurrent));
	PyDict_SetItemString(result, "bIsHighCurrent", PyInt_FromLong((int)d.BIsHighCurrent));
	PyDict_SetItemString(result, "ifAIsFifo", PyInt_FromLong((int)d.IFAIsFifo));
	PyDict_SetItemString(result, "ifAIsFifoTar", PyInt_FromLong((int)d.IFAIsFifoTar));
	PyDict_SetItemString(result, "ifAIsFastSer", PyInt_FromLong((int)d.IFAIsFastSer));
	PyDict_SetItemString(result, "aIsVCP", PyInt_FromLong((int)d.AIsVCP));
	PyDict_SetItemString(result, "ifBIsFifo", PyInt_FromLong((int)d.IFBIsFifo));
	PyDict_SetItemString(result, "ifBIsFifoTar", PyInt_FromLong((int)d.IFBIsFifoTar));
	PyDict_SetItemString(result, "ifBIsFastSer", PyInt_FromLong((int)d.IFBIsFastSer));
	PyDict_SetItemString(result, "bIsVCP", PyInt_FromLong((int)d.BIsVCP));

	PyDict_SetItemString(result, "useExtOsc", PyInt_FromLong((int)d.UseExtOsc));
	PyDict_SetItemString(result, "highDriveIOs", PyInt_FromLong((int)d.HighDriveIOs));
	PyDict_SetItemString(result, "endpointSize", PyInt_FromLong((int)d.EndpointSize));
	PyDict_SetItemString(result, "pullDownEnableR", PyInt_FromLong((int)d.PullDownEnableR));
	PyDict_SetItemString(result, "serNumEnableR", PyInt_FromLong((int)d.SerNumEnableR));
	PyDict_SetItemString(result, "invertTXD", PyInt_FromLong((int)d.InvertTXD));
	PyDict_SetItemString(result, "invertRXD", PyInt_FromLong((int)d.InvertRXD));
	PyDict_SetItemString(result, "invertRTS", PyInt_FromLong((int)d.InvertRTS));
	PyDict_SetItemString(result, "invertCTS", PyInt_FromLong((int)d.InvertCTS));
	PyDict_SetItemString(result, "invertDTR", PyInt_FromLong((int)d.InvertDTR));
	PyDict_SetItemString(result, "invertDSR", PyInt_FromLong((int)d.InvertDSR));
	PyDict_SetItemString(result, "invertDCD", PyInt_FromLong((int)d.InvertDCD));
	PyDict_SetItemString(result, "invertRI", PyInt_FromLong((int)d.InvertRI));
	PyDict_SetItemString(result, "cbus0", PyInt_FromLong((int)d.Cbus0));
	PyDict_SetItemString(result, "cbus1", PyInt_FromLong((int)d.Cbus1));
	PyDict_SetItemString(result, "cbus2", PyInt_FromLong((int)d.Cbus2));
	PyDict_SetItemString(result, "cbus3", PyInt_FromLong((int)d.Cbus3));
	PyDict_SetItemString(result, "cbus4", PyInt_FromLong((int)d.Cbus4));
	PyDict_SetItemString(result, "rIsD2XX", PyInt_FromLong((int)d.RIsD2XX));

	PyDict_SetItemString(result, "pullDownEnable7", PyInt_FromLong((int)d.PullDownEnable7));
	PyDict_SetItemString(result, "serNumEnable7", PyInt_FromLong((int)d.SerNumEnable7));
	PyDict_SetItemString(result, "alSlowSlew", PyInt_FromLong((int)d.ALSlowSlew));
	PyDict_SetItemString(result, "alSchmittInput", PyInt_FromLong((int)d.ALSchmittInput));
	PyDict_SetItemString(result, "alDriveCurrent", PyInt_FromLong((int)d.ALDriveCurrent));
	PyDict_SetItemString(result, "ahSlowSlew", PyInt_FromLong((int)d.AHSlowSlew));
	PyDict_SetItemString(result, "ahSchmittInput", PyInt_FromLong((int)d.AHSchmittInput));
	PyDict_SetItemString(result, "ahDriveCurrent", PyInt_FromLong((int)d.AHDriveCurrent));
	PyDict_SetItemString(result, "blSlowSlew", PyInt_FromLong((int)d.BLSlowSlew));
	PyDict_SetItemString(result, "blSchmittInput", PyInt_FromLong((int)d.BLSchmittInput));
	PyDict_SetItemString(result, "blDriveCurrent", PyInt_FromLong((int)d.BLDriveCurrent));
	PyDict_SetItemString(result, "bhSlowSlew", PyInt_FromLong((int)d.BHSlowSlew));
	PyDict_SetItemString(result, "bhSchmittInput", PyInt_FromLong((int)d.BHSchmittInput));
	PyDict_SetItemString(result, "bhDriveCurrent", PyInt_FromLong((int)d.BHDriveCurrent));
	PyDict_SetItemString(result, "ifAIsFifo7", PyInt_FromLong((int)d.IFAIsFifo7));
	PyDict_SetItemString(result, "ifAIsFifoTar7", PyInt_FromLong((int)d.IFAIsFifoTar7));
	PyDict_SetItemString(result, "ifAIsFastSer7", PyInt_FromLong((int)d.IFAIsFastSer7));
	PyDict_SetItemString(result, "aIsVCP7", PyInt_FromLong((int)d.AIsVCP7));
	PyDict_SetItemString(result, "ifBIsFifo7", PyInt_FromLong((int)d.IFBIsFifo7));
	PyDict_SetItemString(result, "ifBIsFifoTar7", PyInt_FromLong((int)d.IFBIsFifoTar7));
	PyDict_SetItemString(result, "ifBIsFastSer7", PyInt_FromLong((int)d.IFBIsFastSer7));
	PyDict_SetItemString(result, "bIsVCP7", PyInt_FromLong((int)d.BIsVCP7));
	PyDict_SetItemString(result, "powerSaveEnable", PyInt_FromLong((int)d.PowerSaveEnable));

	PyDict_SetItemString(result, "pullDownEnable8", PyInt_FromLong((int)d.PullDownEnable8));
	PyDict_SetItemString(result, "serNumEnable8", PyInt_FromLong((int)d.SerNumEnable8));
	PyDict_SetItemString(result, "aSlowSlew", PyInt_FromLong((int)d.ASlowSlew));
	PyDict_SetItemString(result, "aSchmittInput", PyInt_FromLong((int)d.ASchmittInput));
	PyDict_SetItemString(result, "aDriveCurrent", PyInt_FromLong((int)d.ADriveCurrent));
	PyDict_SetItemString(result, "bSlowSlew", PyInt_FromLong((int)d.BSlowSlew));
	PyDict_SetItemString(result, "bSchmittInput", PyInt_FromLong((int)d.BSchmittInput));
	PyDict_SetItemString(result, "bDriveCurrent", PyInt_FromLong((int)d.BDriveCurrent));
	PyDict_SetItemString(result, "cSlowSlew", PyInt_FromLong((int)d.CSlowSlew));
	PyDict_SetItemString(result, "cSchmittInput", PyInt_FromLong((int)d.CSchmittInput));
	PyDict_SetItemString(result, "cDriveCurrent", PyInt_FromLong((int)d.CDriveCurrent));
	PyDict_SetItemString(result, "dSlowSlew", PyInt_FromLong((int)d.DSlowSlew));
	PyDict_SetItemString(result, "dSchmittInput", PyInt_FromLong((int)d.DSchmittInput));
	PyDict_SetItemString(result, "dDriveCurrent", PyInt_FromLong((int)d.DDriveCurrent));
	PyDict_SetItemString(result, "aRIIsTXDEN", PyInt_FromLong((int)d.ARIIsTXDEN));
	PyDict_SetItemString(result, "bRIIsTXDEN", PyInt_FromLong((int)d.BRIIsTXDEN));
	PyDict_SetItemString(result, "cRIIsTXDEN", PyInt_FromLong((int)d.CRIIsTXDEN));
	PyDict_SetItemString(result, "dRIIsTXDEN", PyInt_FromLong((int)d.DRIIsTXDEN));
	PyDict_SetItemString(result, "aIsVCP8", PyInt_FromLong((int)d.AIsVCP8));
	PyDict_SetItemString(result, "bIsVCP8", PyInt_FromLong((int)d.BIsVCP8));
	PyDict_SetItemString(result, "cIsVCP8", PyInt_FromLong((int)d.CIsVCP8));
	PyDict_SetItemString(result, "dIsVCP8", PyInt_FromLong((int)d.DIsVCP8));

	PyDict_SetItemString(result, "pullDownEnableH", PyInt_FromLong((int)d.PullDownEnableH));
	PyDict_SetItemString(result, "serNumEnableH", PyInt_FromLong((int)d.SerNumEnableH));
	PyDict_SetItemString(result, "acSlowSlewH", PyInt_FromLong((int)d.ACSlowSlewH));
	PyDict_SetItemString(result, "acSchmittInputH", PyInt_FromLong((int)d.ACSchmittInputH));
	PyDict_SetItemString(result, "acDriveCurrentH", PyInt_FromLong((int)d.ACDriveCurrentH));
	PyDict_SetItemString(result, "adSlowSlewH", PyInt_FromLong((int)d.ADSlowSlewH));
	PyDict_SetItemString(result, "adSchmittInputH", PyInt_FromLong((int)d.ADSchmittInputH));
	PyDict_SetItemString(result, "adDriveCurrentH", PyInt_FromLong((int)d.ADDriveCurrentH));
	PyDict_SetItemString(result, "cbus0H", PyInt_FromLong((int)d.Cbus0H));
	PyDict_SetItemString(result, "cbus1H", PyInt_FromLong((int)d.Cbus1H));
	PyDict_SetItemString(result, "cbus2H", PyInt_FromLong((int)d.Cbus2H));
	PyDict_SetItemString(result, "cbus3H", PyInt_FromLong((int)d.Cbus3H));
	PyDict_SetItemString(result, "cbus4H", PyInt_FromLong((int)d.Cbus4H));
	PyDict_SetItemString(result, "cbus5H", PyInt_FromLong((int)d.Cbus5H));
	PyDict_SetItemString(result, "cbus6H", PyInt_FromLong((int)d.Cbus6H));
	PyDict_SetItemString(result, "cbus7H", PyInt_FromLong((int)d.Cbus7H));
	PyDict_SetItemString(result, "cbus8H", PyInt_FromLong((int)d.Cbus8H));
	PyDict_SetItemString(result, "cbus9H", PyInt_FromLong((int)d.Cbus9H));
	PyDict_SetItemString(result, "isFifoH", PyInt_FromLong((int)d.IsFifoH));
	PyDict_SetItemString(result, "isFifoTarH", PyInt_FromLong((int)d.IsFifoTarH));
	PyDict_SetItemString(result, "isFastSerH", PyInt_FromLong((int)d.IsFastSerH));
	PyDict_SetItemString(result, "isFt1248H", PyInt_FromLong((int)d.IsFT1248H));
	PyDict_SetItemString(result, "ft1248CpolH", PyInt_FromLong((int)d.FT1248CpolH));
	PyDict_SetItemString(result, "ft1248LsbH", PyInt_FromLong((int)d.FT1248LsbH));
	PyDict_SetItemString(result, "ft1248FlowControlH", PyInt_FromLong((int)d.FT1248FlowControlH));
	PyDict_SetItemString(result, "isVCPH", PyInt_FromLong((int)d.IsVCPH));
	PyDict_SetItemString(result, "powerSaveEnableH", PyInt_FromLong((int)d.PowerSaveEnableH));

	return result;
}

//    FT_HANDLE ftHandle,
//	LPDWORD lpdwSize
static PyObject*
ftobj_EE_UASize(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	DWORD l;

	if (!PyArg_ParseTuple(ao, ":eeUASize")) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_EE_UASize(fo->handle, &l);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	return PyInt_FromLong((int)l);
}

//  FT_HANDLE ftHandle,
//	PUCHAR pucData,
//	DWORD dwDataLen
static PyObject*
ftobj_EE_UAWrite(Ftobj *fo, PyObject *ao) {
	FT_STATUS s;
	char *d;
	int l;

	if (!PyArg_ParseTuple(ao, "s#:eeUAWrite", &d, &l)) return NULL;

	Py_BEGIN_ALLOW_THREADS
	s = FT_EE_UAWrite(fo->handle, d, l);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		return NULL;
	}

	Py_INCREF(Py_None); return Py_None;
}

//    FT_HANDLE ftHandle,
//	PUCHAR pucData,
//	DWORD dwDataLen,
//	LPDWORD lpdwBytesRead
static PyObject*
ftobj_EE_UARead(Ftobj *fo, PyObject *ao) {
	PyObject *result = NULL;
	FT_STATUS s;
	int l; // bytes to read
	DWORD r; // bytes returned
	char *b;

	if (!PyArg_ParseTuple(ao, "i:eeUARead", &l)) return NULL;
	b = (char *)PyMem_Malloc(l*sizeof(char));
	if (!b) return PyErr_NoMemory();

	Py_BEGIN_ALLOW_THREADS
	s = FT_EE_UARead(fo->handle, b, l, &r);
	Py_END_ALLOW_THREADS
	if (!FT_SUCCESS(s)) {
		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
		goto end;
	}

	result = PyString_FromStringAndSize(b, r);

end:
	PyMem_Free(b);
	return result;
}

//
// Win32 API
//

#ifdef WIN32_API

//	PVOID					pvArg1,
//	DWORD					dwAccess,
//	DWORD					dwShareMode,
//	LPSECURITY_ATTRIBUTES	lpSecurityAttributes,
//	DWORD					dwCreate,
//	DWORD					dwAttrsAndFlags,
//	HANDLE					hTemplate
static PyObject*
ftobj_W32_CreateFile(Ftobj *fo, PyObject *ao) {
	FT_HANDLE h;
	char *n;
	int a, f;
//	int a, m, c, f;
//	PyObject *o0, *o1;

//	if (!PyArg_ParseTuple(ao, "siiOiiO:w32CreateFile", &n, &a, &m, &o0, &c, &f, &o1)) return NULL;
	if (!PyArg_ParseTuple(ao, "sii:w32CreateFile", &n, &a, &f)) return NULL;

	h = FT_W32_CreateFile(
		(void*)n, (DWORD)a, 0,
		(LPSECURITY_ATTRIBUTES)NULL,
		OPEN_EXISTING, (DWORD)f, (HANDLE)NULL
	);

	// if (h == INVALID_HANDLE_VALUE) PyErr_Warn(PyExc_RuntimeWarning, "Invalid handle.");

	return (PyObject *)ftobj_alloc(h);
}


////BOOL FT_W32_ReadFile(
//// FT_HANDLE ftHandle,
////    LPVOID lpBuffer,
////    DWORD nBufferSize,
////    LPDWORD lpBytesReturned,
////	LPOVERLAPPED lpOverlapped
//static PyObject*
//ftobj_W32_ReadFile(Ftobj *fo, PyObject *ao) {
//	PyObject *result = NULL;
//	int b, o = 0; // bytes requested, overlap
//	DWORD r; // bytes read
//	BOOL s; // result
//	char *buf = NULL;
//
//	if (!PyArg_ParseTuple(ao, "i|i:w32ReadFile", &b, &o)) return NULL;
//
//	buf = PyMem_Malloc(b);
//	if (!buf) return PyErr_NoMemory();
//
//	if (o) {
//		fo->overlap.Internal = 0;
//		fo->overlap.InternalHigh = 0;
//		fo->overlap.Offset = 0;
//		fo->overlap.OffsetHigh = 0;
//		fo->overlap.hEvent = 0;
//	}
//
//	s = FT_W32_ReadFile(fo->handle, buf, b, &r, (o) ? fo->overlap : NULL);
//
//	if (s) result = PyString_FromStringAndSize(buf, (int)r);
//	else {
//		result = Py_None;
//		Py_INCREF(Py_None);
//	}
//
//	PyMem_Free(buf);
//	return result;
//}

#endif // WIN32_API



static PyObject*
ftobj_getattr(Ftobj *fo, char *name) {
	if (!fo) {
		PyErr_BadInternalCall();
		return NULL;
	}

/*
	if (strcmp(name, "typecode") == 0) {
		char tc = a->ob_descr->typecode;
		return PyString_FromStringAndSize(&tc, 1);
	}

	if (strcmp(name, "itemsize") == 0) {
		return PyInt_FromLong((long)a->ob_descr->itemsize);
	}
	*/

	if (strcmp(name, "handle") == 0) {
		return PyInt_FromLong((long)fo->handle);
	}

/*	if (strcmp(name, "last") == 0) {
		return Py_BuildValue("(ii)", fo->i, fo->o);
	}
*/
	return Py_FindMethod(ftobj_methods, (PyObject*)fo, name);
}

//static PyObject*
//new_ppbdmport(PyObject *so, PyObject *ao) {
//	int b = DEFAULT_PORT;
//#ifdef WIN32
//	HANDLE h;
//#else
//	byte r;
//#endif
//
//	if (!PyArg_ParseTuple(ao, "|i", &b)) return NULL;
//
//#ifdef WIN32
//	// tell giveio we need access
//	if (!_giveio) {
//		h = CreateFile("\\\\.\\giveio", GENERIC_READ, 0, NULL,
//			OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
//		if (h==INVALID_HANDLE_VALUE) {
//			PyErr_SetString(Ppbdmerror, "giveio driver not found");
//			return NULL;
//		}
//		CloseHandle(h);
//		_giveio = 1;
//	}
//#else
//	if (!_ioperm) {
//		r = ioperm(b, 3, 1);
//		if (r) {
//			PyErr_SetString(Ppbdmerror, "giveio driver not found");
//			return NULL;
//		}
//		_ioperm = 1;
//	}
//#endif
//
//	return (PyObject*) ftobj_alloc(b);
//}

static PyMethodDef d2xx_methods[] = {
	{"open", (PyCFunction)ftobj_Open, METH_VARARGS,
		"Open the device and return a handle which will be used for subsequent accesses."},
	{"openEx", (PyCFunction)ftobj_OpenEx, METH_VARARGS,
		"Open the named device and return a handle which will be used for subsequent accesses. "
		"The device name can be its serial number or device description."},
	{"listDevices", (PyCFunction)ftobj_ListDevices, METH_VARARGS,
		"Get information concerning the devices currently connected. "
		"This function can return such information as the number of devices connected, "
		"and device strings such as serial number and product description."},
	{"getLibraryVersion", (PyCFunction)ftobj_GetLibraryVersion, METH_VARARGS,
		"Returns D2XX DLL version number."},
	{"createDeviceInfoList", (PyCFunction)ftobj_CreateDeviceInfoList, METH_VARARGS,
		"Builds a device information list and returns the number of D2XX devices connected to "
		"the system. The list contains information about both unopen and open devices."},
	{"getDeviceInfoDetail", (PyCFunction)ftobj_GetDeviceInfoDetail, METH_VARARGS,
		"returns an entry from the device information list."},

	{"rescan", (PyCFunction)ftobj_Rescan, METH_VARARGS,
		"Scan for hardware changes - can be of use when trying to recover devices programatically."},
	{"reload", (PyCFunction)ftobj_Reload, METH_VARARGS,
		"forces a reload of the driver for devices with a specific VID and PID combination."},

#ifdef WIN32_API
	{"w32CreateFile", (PyCFunction)ftobj_W32_CreateFile, METH_VARARGS,
		"Open the named device and return a handle that will be used for subsequent accesses. "
		"The device name can be its serial number or device description."},
#endif

#ifndef WIN32
	{"setVIDPID", (PyCFunction)ftobj_SetVIDPID, METH_VARARGS,
		"Set VID and PID."},
	{"getVIDPID", (PyCFunction)ftobj_GetVIDPID, METH_VARARGS,
		"Get VID and PID."},
#endif
	{NULL, NULL, 0, NULL}
};

static PyTypeObject Ftobj_Type = {
	PyObject_HEAD_INIT(NULL)
	0,
	"FtobjType",
	sizeof(Ftobj),
	0,
	(destructor)ftobj_dealloc,		/* tp_dealloc */
	0, /* (printfunc)array_print, */			/* tp_print */
	(getattrfunc)ftobj_getattr,		/* tp_getattr */
	0,					/* tp_setattr */
	0,					/* tp_compare */
	0, /* (reprfunc)array_repr,*/			/* tp_repr */
	0,					/* tp_as _number*/
	0, /* &ftobj_as_sequence,*/			/* tp_as _sequence*/
	0,					/* tp_as_mapping*/
	0, 					/* tp_hash */
	0, /* (ternaryfunc)ftobj_call_,	*/				/* tp_call */
	0,					/* tp_str */
	0,					/* tp_getattro */
	0,					/* tp_setattro */
	0, /* &array_as_buffer,*/			/* tp_as _buffer*/
	0, /* Py_TPFLAGS_DEFAULT, */			/* tp_flags */
	0, /* arraytype_doc,	*/			/* tp_doc */
 	0,					/* tp_traverse */
	0,					/* tp_clear */
	0,					/* tp_richcompare */
};

#ifndef PyMODINIT_FUNC	/* declarations for DLL import/export */
#define PyMODINIT_FUNC void
#endif
PyMODINIT_FUNC
init_d2xx() {
	PyObject *m, *d;

#if PY_MAJOR_VERSION == 2
	Ftobj_Type.ob_type = &PyType_Type;
	m = Py_InitModule("_d2xx", d2xx_methods);
#elif PY_MAJOR_VERSION == 3

	Ftobj_Type.tp_new = PyType_GenericNew; // &PyType_Type;
	if (PyType_Ready(&Ftobj_Type) < 0)
		return;

	m = Py_InitModule3("_d2xx", d2xx_methods, "Python bindings for the FTDI D2XX library");

	if (m == NULL)
		return;

	Py_INCREF(&Ftobj_Type);
#else
	#error "Unsupported Python version"
#endif

	d = PyModule_GetDict(m);
	Fterr = PyErr_NewException("_d2xx.Error", PyExc_Exception, NULL);
	PyDict_SetItemString(d, "Error", Fterr);
	PyDict_SetItemString(d, "FtobjType", (PyObject*) &Ftobj_Type);
}

//static PyObject*
//ftobj_reset(Ftobj *fo, PyObject *ao) {
//
//	int t = 0;
//
//	if (!fo) {
//		PyErr_BadInternalCall();
//		return NULL;
//	}
//
//	if (!PyArg_ParseTuple(ao, "|i", &t)) return NULL;
//
////	Sleep(1); // intergap
////	for (i=0; i<8; ++i) {
////		outportb(SK_P, SK_DN); // SO out
////
////		if (to_si & (1<<(7-i))) outportb(SI_P, 1<<SI_B); // write SI
////		else outportb(SI_P, 0<<SI_B);
////
////		if (inportb(SO_P) & (1<<SO_B)) fr_so |= 1<<(7-i); // read SO
////
////		outportb(SK_P, SK_UP); // SI in
////	}
//
//	Py_INCREF(Py_None); return Py_None;
//}

//static PyObject*
//ftobj_transfer(Ftobj *fo, PyObject *ao) {
//	int i, m, t, r = 0;
//
//	if (!PyArg_ParseTuple(ao, "i:transfer", &m)) {
//		PyErr_BadArgument();
//		return NULL;
//	}
//
//	// asume dsclk is down
//
//	t = inportb((word)(fo->port + DSI_P)); // also DSCLK_P
//	for (i=0; i<17; ++i) {
//		if (m & 1<<(16-i)) t |= 1<<DSI;
//		else t &= ~(1<<DSI);
//		outportb((word)(fo->port + DSI_P), (byte) t & 0xff);
//		t |= 1<<DSCLK;
//		outportb((word)(fo->port + DSCLK_P), (byte) t & 0xff);
////		Sleep(100);
//		t &= ~(1<<DSCLK);
//		outportb((word)(fo->port + DSCLK_P), (byte) t & 0xff);
//		r |= (inportb((word)(fo->port + DSO_P)) & 1<<DSO) ? 0 : 1;
//		r <<= 1;
//	}
//
//	return PyInt_FromLong(r>>1);
//}
//
///*
//// expand this to accept sequences
//static PyObject*
//ftobj_call_(Ftobj *fo, PyObject *ao, PyObject *ko) // const char *pn, const uwioconf *xc) {
//	int al;
//	if (!fo || !PyTuple_Check(ao)) {
//		PyErr_BadInternalCall();
//		return NULL;
//	}
//
//	al = PyTuple_Size(ao);
//
//	if (al==0) {
//		PyErr_BadArgument();
//		return NULL;
//	}
//	else if (al==1) return ftobj_call_obj(fo, PyTuple_GetItem(ao, 0));
//	else return ftobj_call_obj(fo, ao);
//}
//
//static PyObject*
//ftobj_call_obj(Ftobj *fo, PyObject *ao) {
//	if (PyInt_Check(ao)) {
//		fo->i = (byte) (PyInt_AsLong(ao) & 0xff);
//		fo->o = (byte) (ftobj_sio(fo, fo->i) & 0xff);
//		return PyInt_FromLong((long)fo->o);
//	}
//	else if (PyString_Check(ao)) {
//		int i;
//		const int l = PyString_Size(ao);
//		const char *s = PyString_AsString(ao);
//		char r[l];
//
//		for (i=0; i<l; ++i) r[i] = (char) (ftobj_sio(fo, s[i]) & 0xff);
//		fo->i = (byte) s[l-1];
//		fo->o = (byte) r[l-1];
//		return PyString_FromStringAndSize(r, l);
//	}
//	else return ftobj_call_seq(fo, ao);
//}
//
//static PyObject*
//ftobj_call_seq(Ftobj *fo, PyObject *ao) {
//	int i, l;
//	PyObject *ro = NULL, *tmp;
//
//	if (!PyTuple_Check(ao) && !PyList_Check(ao)) {
//		PyErr_SetString(Uwioerror, "type not supported");
//		return NULL;
//	}
//
//	l = PyObject_Length(ao);
//	if (l == -1) return NULL;
//
//	if (PyTuple_Check(ao)) {
//		ro = PyTuple_New(l);
//		if (!ro) return PyErr_NoMemory();
//
//		for (i=0; i<l; ++i) {
//			tmp = ftobj_call_obj(fo, PyTuple_GetItem(ao, i));
//			if (!tmp) {
//				Py_DECREF(ro);
//				return NULL;
//			}
//			PyTuple_SetItem(ro, i, tmp);
//		}
//	}
//	else if (PyList_Check(ao)) {
//		ro = PyList_New(l);
//		if (!ro) return PyErr_NoMemory();
//
//		for (i=0; i<l; ++i) {
//			tmp = ftobj_call_obj(fo, PyList_GetItem(ao, i));
//			if (!tmp) {
//				Py_DECREF(ro);
//				return NULL;
//			}
//			PyList_SetItem(ro, i, tmp);
//		}
//	}
//
//	return ro;
//}
//*/

////	PVOID pArg1,
////	PVOID pArg2,
////	DWORD Flags
//static PyObject*
//ftobj_ListDevices(Ftobj *fo, PyObject *ao) {
//	PyObject *result = NULL;
//	int r = 0;
//	FT_STATUS s;
//	DWORD n;
//	char **ba = NULL; //, *bd = NULL;
//	unsigned i;
//
////	if (!PyArg_ParseTuple(ao, "i", &a)) return NULL;
////	if (a & FT_LIST_NUMBER_ONLY) {
////		if (!FT_SUCCESS(s = FT_ListDevices(&n, NULL, FT_LIST_NUMBER_ONLY))) {
////			PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
////			return NULL;
////		}
////		else return PyInt_FromLong(n);
////	}
//
//	// when null, the default is to open by serial number
//	if (!PyArg_ParseTuple(ao, "|i:listDevices", &r)) return NULL;
//	if (r == 0) r = FT_OPEN_BY_SERIAL_NUMBER;
//
//	// search for number of devices first
//	s = FT_ListDevices(&n, NULL, FT_LIST_NUMBER_ONLY);
//	if (!FT_SUCCESS(s)) {
//		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
//		return NULL;
//	}
//
//	if (!n) return PyTuple_New(0);
//
//	ba = (char **)PyMem_Malloc((n+1)*sizeof(char*)); // last is NULL
//	if (!ba) return PyErr_NoMemory();
//	for (i=0; i<=n; ++i) ba[i] = NULL;
//
////	bd = (char *)PyMem_Malloc(n*DESCRIPTION_SIZE*sizeof(char));
////	if (!bd) {
////		PyMem_Free(ba);
////		return PyErr_NoMemory();
////	}
//
//	for (i=0; i<n; ++i) {
//		// ba[i] = &bd[i*DESCRIPTION_SIZE];
//		// ba[i] = bd + i*DESCRIPTION_SIZE;
//		ba[i] = (char *)PyMem_Malloc(DESCRIPTION_SIZE*sizeof(char));
//		if (ba[i] == NULL) {
//			PyErr_SetNone(PyExc_MemoryError);
//			goto end;
//		}
//		fprintf(stderr, "%x\n", ba[i]);
//	}
////	ba[l] = NULL;
//
//	// for (i=0; i<l; ++i) {
//	// s = FT_ListDevices((PVOID)i, ba[i], FT_LIST_BY_INDEX | (DWORD)r);
//	s = FT_ListDevices(ba, &n, FT_LIST_ALL | (DWORD)r);
//	if (!FT_SUCCESS(s)) {
//		PyErr_SetObject(Fterr, Py_BuildValue("(is)", s, Ftstat[s].description));
//		goto end;
//	}
// 	//}
//
////	result = PyInt_FromLong((int)n);
//
//	result = PyTuple_New(n);
//	if (result == NULL) {
//		PyErr_SetNone(PyExc_MemoryError);
//		goto end;
//	}
//
//	for (i=0; i<n; ++i) PyTuple_SetItem(result, i, PyString_FromString(ba[i]));
//
//end:
////	PyMem_Free(bd);
//	for (i=0; i<n; ++i) PyMem_Free(ba[n-1-i]);
//	PyMem_Free(ba);
//	return result;
//}

//typedef struct _OVERLAPPED {
//        DWORD Internal;
//        DWORD InternalHigh;
//        DWORD Offset;
//        DWORD OffsetHigh;
//        HANDLE hEvent;
//}
