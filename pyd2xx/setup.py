from distutils.core import setup, Extension
from conf import *;

setup(
	name='pyd2xx',
	version='1.8',
	description='FTDI D2XX Python driver',
	license='LGPL',
	author='Pablo Bleyer',
	author_email='pablo@bleyer.org',
	url='http://bleyer.org',
	packages=['d2xx'],
	ext_modules=[
		Extension('d2xx._d2xx', ['d2xx/_d2xx.c'],
			include_dirs=[FTDI+'/'+TARGET],
			library_dirs=[FTDI], libraries=['ftd2xx']
		)
	],
	data_files=[('Lib/site-packages/d2xx', ['./d2xx/LGPL.txt', FTDI+'/'+TARGET+'/'+ARCH+'/ftd2xx.dll'])]
)
